#include "hdecode.h"

void parse_input(int argc, char *argv[], int fd[]) {
    if(argc == 2) {
        if(!strcmp(argv[1], "-")) {
            fd[0] = STDIN_FILENO;
            fd[1] = STDOUT_FILENO;
            return;
        }
        else {
            if((fd[0] = open(argv[1], O_RDONLY))) {
                fd[1] = STDOUT_FILENO;
                return;
            }
        }
        fprintf(stderr, "file does not exist");
        exit(0);
        return;
    }
    if(argc == 3) {
        if((fd[0] = open(argv[1], O_RDONLY))) {
            fd[1] = open(argv[2], O_CREAT|O_WRONLY|O_TRUNC, 0777);
            return;
        }
        fprintf(stderr, "file does not exist");
        exit(0);
        return;
    }
    fprintf(stderr, "usage: hencode infile [ outfile ]\n");
    exit(0);
    return;
}

int parse_header(int input, int freqs[256]) {
    int byte = 0;
    int freq = 0;
    int i;
    int num_nodes = 0;
    read(input, &num_nodes, 4);
    if(num_nodes) {
        for(i = 0; i < num_nodes; i++) {
            read(input, &byte, 1);
            read(input, &freq, 4);
            freqs[byte] = freq;
        }
    }
    return num_nodes;
}

void insert_node(Node* new_node, Node** head) {
    Node* current = NULL;
    Node* prev = NULL;

    if(*head == NULL) {
        new_node->next = *head;
        *head = new_node;
    }
    else {
        current = *head;
        prev = NULL;
        while(current && (current->frequency < new_node->frequency || (current->frequency == new_node->frequency && current->byte - new_node->byte < 0))) {
            prev = current;
            current = current->next;
        }
        if(prev == NULL) {
            new_node->next = current;
            *head = new_node;
            return;
        }
        prev->next = new_node;
        new_node->next = current;
    }
}

Node* create_huffman_node(char byte, int frequency, Node* left, Node* right) {
    Node *huffman_node = (Node*)malloc(sizeof(Node));
    huffman_node->byte = byte;
    huffman_node->frequency = frequency;
    huffman_node->left = left;
    huffman_node->right = right;
    return huffman_node;
}

Node* create_list_node(char byte, int frequency) {
    Node *list_node = (Node*)malloc(sizeof(Node));
    list_node->byte = byte;
    list_node->frequency = frequency;
    list_node->left = NULL;
    list_node->right = NULL;
    return list_node;
}

Node* pop_node(Node **head) {
    Node* popped = NULL;
    popped = *head;
    *head = (*head)->next;
    return popped;
}

Node* create_tree(Node *head, int *num_nodes) {
    Node *left_node = NULL;
    Node *right_node = NULL;
    Node *huff_node = NULL;
    huff_node = head;
    while(*num_nodes > 1) {
        left_node = pop_node(&head);
        right_node = pop_node(&head);
        huff_node = create_huffman_node(0, left_node->frequency + right_node->frequency, left_node, right_node);
        insert_node(huff_node, &head);
        *num_nodes -= 1;
    }
    return huff_node;
}

void traverse_tree(Node *tree, char code[], char map[256][256], int current) {
    if(tree) {
        if(tree->left) {
            code[current] = '0';
            traverse_tree(tree->left, code, map, current + 1);
        }
    
        if(tree->right) {
            code[current] = '1';
            traverse_tree(tree->right, code, map, current + 1);
        }
    
        if(tree->right == NULL && tree->left == NULL) {
            code[current] = '\0';
            strcpy(map[(int)tree->byte], code);
        }
    }
}

void free_tree(Node *tree) {
    if(!tree) {
        return;
    }
    free_tree(tree->left);
    free_tree(tree->right);

    free(tree);
}

void generate_code(int output, char* code_string, unsigned char *current_byte, int *bit_count) {
    int i;
    int length = 0;
    length = strlen(code_string);
    for(i = 0; i < length; i++, (*bit_count)++) {
        if(*bit_count == 8) {
            write(output, current_byte, 1);
            fsync(output);
            *bit_count = 0;
            *current_byte = 0;
        }
        *current_byte |= (code_string[i] - '0') << (7 - *bit_count);
    }
}

void decode_file(Node *root, Node *tree, int input, int output, int num_chars) {
    char byte;
    char current_byte;
    int bytes_read;
    int mask = 0;
    while((bytes_read = read(input, &byte, 1) > 0)) {
        mask = 128;
        while(mask > 0 && num_chars) {
            current_byte = tree->byte;
            if(byte & mask) {
                if(tree->right) {
                    tree = tree->right;
                }
                current_byte = tree->byte;
            }
            else {
                if(tree->left) {
                    tree = tree->left;
                }
                current_byte = tree->byte;
            }
            if(tree->left == NULL && tree->right == NULL) {
                write(output, &current_byte, sizeof(char));
                tree = root;
                num_chars--;
            }
            mask >>= 1;
        }
    }
    current_byte = root->byte;
    while(num_chars) {
        write(output, &current_byte, sizeof(char));
        num_chars--;
    }
}

int main(int argc, char *argv[]) {
    int i;
    int j;
    int freqs[256] = {0};
    int fd[2] = {0};
    char code[256] = {0};
    char map[256][256] = {{0}};
    int num_nodes = 0;
    int num_chars = 0;
    Node *head = NULL;
    Node *tree = NULL;
    int input = 0;
    int output = 0;
    for(i = 0; i < 256; i++) {
        for(j = 0; j < 256; j++) {
            map[i][j] = 0;
        }
    }
    parse_input(argc, argv, fd);
    input = fd[0];
    output = fd[1];
    num_nodes = parse_header(input, freqs);
    if(num_nodes > 0) {
        for(i = 0; i < 256; i++) {
            if(freqs[i]) {
                insert_node(create_list_node(i, freqs[i]), &head);
                num_chars += freqs[i];
            }
        }
        tree = create_tree(head, &num_nodes);
        traverse_tree(tree, code, map, 0);
        decode_file(tree, tree, input, output, num_chars);
        free_tree(tree);
    }
    close(input);
    close(output);
    return 0;
}
