#include <fcntl.h> 
#include <stdio.h> 
#include <string.h>
#include <unistd.h> 
#include <stdlib.h>
#include <errno.h>

typedef struct node Node;

struct node {
    char byte;
    int frequency;
    Node *left;
    Node *right;
    Node *next;
};

void parse_input(int argc, char *argv[],  int fd[]);

void count_freqs(int input, int freqs[]);

void insert_node(Node* new_node, Node** head);

Node* create_huffman_node(char byte, int frequency, Node* left, Node* right);

Node* create_list_node(char byte, int frequency);

Node* pop_node(Node **head);

Node* create_tree(Node *head, int *num_nodes);

void traverse_tree(Node *tree, char code[], char map[256][256], int current);

void free_tree(Node *tree);

void encode_file(char map[256][256], int freqs[], int num_bytes, int output, int input);
