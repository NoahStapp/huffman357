#include "hencode.h"

void parse_input(int argc, char *argv[], int fd[]) {
    if(argc == 2) {
        if((fd[0] = open(argv[1], O_RDONLY))) {
            fd[1] = STDOUT_FILENO;
            return;
        }
        fprintf(stderr, "file does not exist");
        exit(0);
        return;
    }
    if(argc == 3) {
        if((fd[0] = open(argv[1], O_RDONLY))) {
            fd[1] = open(argv[2], O_CREAT|O_WRONLY|O_TRUNC, 0777);
            return;
        }
        fprintf(stderr, "file does not exist");
        exit(0);
        return;
    }
    fprintf(stderr, "usage: hencode infile [ outfile ]\n");
    exit(0);
    return;
}

void count_freqs(int input, int freqs[]) {
    int byte = 0;
    int bytes_read = 0;
    while((bytes_read = read(input, &byte, 1)) > 0) {
        freqs[byte] += 1;
    }
    lseek(input, 0, SEEK_SET);
}

void insert_node(Node* new_node, Node** head) {
    Node* current = NULL;
    Node* prev = NULL;
    if(*head == NULL) {
        new_node->next = *head;
        *head = new_node;
    }
    else {
        current = *head;
        prev = NULL;
        while(current && (current->frequency < new_node->frequency || (current->frequency == new_node->frequency && current->byte - new_node->byte < 0))) {
            prev = current;
            current = current->next;
        }
        if(prev == NULL) {
            new_node->next = current;
            *head = new_node;
            return;
        }
        prev->next = new_node;
        new_node->next = current;
    }
}

Node* create_huffman_node(char byte, int frequency, Node* left, Node* right) {
    Node *huffman_node = (Node*)malloc(sizeof(Node));
    huffman_node->byte = byte;
    huffman_node->frequency = frequency;
    huffman_node->left = left;
    huffman_node->right = right;
    return huffman_node;
}

Node* create_list_node(char byte, int frequency) {
    Node *list_node = (Node*)malloc(sizeof(Node));
    list_node->byte = byte;
    list_node->frequency = frequency;
    list_node->left = NULL;
    list_node->right = NULL;
    return list_node;
}

Node* pop_node(Node **head) {
    Node* popped = NULL;
    popped = *head;
    *head = (*head)->next;
    return popped;
}

Node* create_tree(Node *head, int *num_nodes) {
    Node *left_node = NULL;
    Node *right_node = NULL;
    Node *huff_node = NULL;
    while(*num_nodes > 1) {
        left_node = pop_node(&head);
        right_node = pop_node(&head);
        huff_node = create_huffman_node(0, left_node->frequency + right_node->frequency, left_node, right_node);
        insert_node(huff_node, &head);
        *num_nodes -= 1;
    }
    return huff_node;
}

void traverse_tree(Node *tree, char code[], char map[256][256], int current) {
    if(tree) {
        if(current >= 256) {
            printf("Current >= 256\n");
            current = 255;
        }
        if(tree->left) {
            code[current] = '0';
            traverse_tree(tree->left, code, map, current + 1);
        }
        
        if(tree->right) {
            code[current] = '1';
            traverse_tree(tree->right, code, map, current + 1);
        }
    
        if(tree->right == NULL && tree->left == NULL) {
            code[current] = '\0';
            strcpy(map[(int)tree->byte], code);
        }
    }
}

void free_tree(Node *tree) {
    if(tree) {
        if(tree->left) {
            free_tree(tree->left);
        }
        if(tree->right) {
            free_tree(tree->right);
        }
        free(tree);
    }
}

void generate_code(int output, char* code_string, unsigned char *current_byte, int *bit_count) {
    int i;
    int length = 0;
    length = strlen(code_string);
    for(i = 0; i < length; i++, (*bit_count)++) {
        if(*bit_count == 8) {
            write(output, current_byte, 1);
            fsync(output);
            *bit_count = 0;
            *current_byte = 0;
        }
        *current_byte |= (code_string[i] - '0') << (7 - *bit_count);
    }
}

void encode_file(char map[256][256], int freqs[], int num_bytes, int input, int output) {
    int i;
    unsigned char byte = 0;
    int bytes_read = 0;
    int bit_count = 0;
    unsigned char current_byte = 0;
    write(output, &num_bytes, sizeof(num_bytes));
    fsync(output);
    for(i = 0; i < 256; i++) {
        if(freqs[i]) {
            write(output, &i, sizeof(char));
            fsync(output);
            write(output, &freqs[i], sizeof(int));
            fsync(output);
        }
    }
    while((bytes_read = read(input, &byte, 1)) > 0) {
        generate_code(output, map[(int)byte], &current_byte, &bit_count);
    }
    if(bit_count) {
        write(output, &current_byte, 1);
        fsync(output);
    }
}

int main(int argc, char *argv[]) {
    int i;
    int j;
    int freqs[256] = {0};
    int fd[2];
    char code[4096] = {0};
    char map[256][256] = {{0}};
    Node *head = NULL;
    Node *tree = NULL;
    int num_nodes = 0;
    int num_bytes = 0;
    int input = 0;
    int output = 0;
    for(i = 0; i < 256; i++) {
        for(j = 0; j < 256; j++) {
            map[i][j] = (char)0;
        }
    }
    parse_input(argc, argv, fd);
    input = fd[0];
    output = fd[1];
    count_freqs(input, freqs);
    for(i = 0; i < 256; i++) {
        if(freqs[i]) {
            insert_node(create_list_node(i, freqs[i]), &head);
            num_nodes++;
        }
    }
    num_bytes = num_nodes;
    if(num_bytes > 0) {
        tree = create_tree(head, &num_nodes);
        traverse_tree(tree, code, map, 0);
        free_tree(tree);
        encode_file(map, freqs, num_bytes, input, output);
    }
    else {
        write(output, &num_bytes, 4);
    }
    close(input);
    close(output);
    return 0;
}
