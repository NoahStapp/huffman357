CC = gcc
CFLAGS = -Wall -ansi -g -pedantic -fsanitize=address -fsanitize=undefined
MAIN1 = hencode
MAIN2 = hdecode
OBJS1 = hencode.o
OBJS2 = hdecode.o

all : $(MAIN1) $(MAIN2)

$(MAIN1) : $(OBJS1) hencode.h
	$(CC) $(CFLAGS) -o $(MAIN1) $(OBJS1)

$(MAIN2) : $(OBJS2) hdecode.h
	$(CC) $(CFLAGS) -o $(MAIN2) $(OBJS2)

hencode.o : hencode.c hencode.h
	$(CC) $(CFLAGS) -c hencode.c

hdecode.o : hdecode.c hdecode.h
	$(CC) $(CFLAGS) -c hdecode.c

clean : 
	rm *.o $(MAIN) 